Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class WorldCupLineup
    Dim startTime As Integer, limitTime As Integer
	Dim rand As New Random(19831983)
	
    Private Class Player
        Public ReadOnly Index As Integer
        Public ReadOnly Flag As Integer
        Public ReadOnly ATK As Integer
        Public ReadOnly DEF As Integer
        Public ReadOnly AGG As Integer
        Sub New(index As Integer, data As String)
            Dim tokens() As String = data.Split(","c)
            Me.Index = index
            Me.Flag = 1 << index
            Me.ATK = CInt(tokens(0))
            Me.DEF = CInt(tokens(1))
            Me.AGG = CInt(tokens(2))
        End Sub
    End Class
    
    Private Class Group
        Public ReadOnly Flag As Integer
        Public ReadOnly Members() As Integer
        Public ReadOnly ATK As Integer
        Public ReadOnly DEF As Integer
        Public ReadOnly AGG As Integer
        Public ReadOnly Count As Integer
        Public ReadOnly Index As Integer
        Sub New(n As Integer, ind As Integer, data As String)
            Dim f As Integer = 0
            Dim c As Integer = 0
            Dim csv() As String = data.Split(" "c)
            Dim ps() As String = csv(0).Split(","c)
            ReDim Members(UBound(ps))
            For i As Integer = 0 To UBound(ps)
                Dim p As Integer = CInt(ps(i))
                Members(i) = p
                f = f Or (1 << p)
                c += 1
            Next i
            Index = ind
            Flag = f
            Count = c
            Dim tokens() As String = csv(1).Split(","c)
            ATK = CInt(tokens(0))
            DEF = CInt(tokens(1))
            AGG = CInt(tokens(2))
        End Sub
    End Class
    
    Private Class Position
        Public PType As Integer = 1
        Public Players As New List(Of Integer)()
        Public Overrides Function ToString() As String
            Dim ret As String = "FMD".Substring(PType, 1) + " "
            For i As Integer = 0 To Players.Count - 1
                If i > 0 Then ret += ","
                ret += CStr(Players(i))
            Next i
            ToString = ret
        End Function
    End Class
    
    Dim players() As Player
    Dim groups() As Group
    
	Public Function selectPositions(playersData() As String, groupsData() As String) As String()
		startTime = Environment.TickCount
        limitTime = startTime + 9700
		Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))
		
        Dim N As Integer = playersData.Length
        Dim G As Integer = groupsData.Length
        
        ReDim players(N - 1)
        For i As Integer = 0 To UBound(playersData)
            players(i) = New Player(i, playersData(i))
        Next i
        
        ReDim groups(G - 1)
        Dim gIndex As Integer = 0
        For i As Integer = 0 To UBound(groupsData)
            Dim grp As New Group(N, gIndex, groupsData(i))
            If grp.Count <= 10 Then
                groups(gIndex) = grp
                gIndex += 1
            End If
        Next i
        G = gIndex
        ReDim Preserve groups(G - 1)
        
        Dim poss(9) As Position
        For i As Integer = 0 To UBound(poss)
            poss(i) = New Position()
        Next i
        
        Dim tea As Team = MakeNew(N)
        Dim score As Integer = 0
        For i As Integer = 0 To 50
            score += Simurate(tea)
        Next i
        Dim maxScore As Integer = score
        Dim tmp As Team = tea.GetCopy()
        Dim time0 As Integer = Environment.TickCount
        Dim timeX As Integer = limitTime - time0
        Const Alpha As Double = 0.000001
        Do
            Dim time1 As Integer = Environment.TickCount
            If time1 > limitTime Then
                Exit Do
            End If
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim z As Integer = 0
            Dim tp As Integer = rand.Next(3)
            Select Case tp
            Case 0
                x = tmp.FW
                y = tmp.DF
                z = 1 << rand.Next(10)
                tmp.FW = x Xor z
                tmp.DF = y Xor z
                tmp.DF = tmp.DF Xor (tmp.FW And tmp.DF)
            Case 1
                x = tmp.FW
                y = tmp.DF
                z = 1 << rand.Next(10)
                tmp.FW = x Xor z
                tmp.DF = y Xor z
                tmp.FW = tmp.FW Xor (tmp.FW And tmp.DF)
            Case Else
                x = rand.Next(30)
                y = (x + rand.Next(29)) Mod 30
                z = tmp.Order(x)
                tmp.Order(x) = tmp.Order(y)
                tmp.Order(y) = z
            End Select
            
            Dim tmpScore As Integer = 0
            For i As Integer = 0 To 50
                tmpScore += Simurate(tmp)
            Next i
            If tmpScore < score Then
                Dim timeY As Integer = time1 - time0
                Dim te As Double = Math.Pow(Alpha, CDbl(timeY) / CDbl(timeX))
                Dim ee As Double = Math.Exp(CDbl(tmpScore - score) / CDbl(maxScore - tmpScore) / te)
                If rand.NextDouble() > ee Then
                    Select Case tp
                    Case 0, 1
                        tmp.FW = x
                        tmp.DF = y
                    Case Else
                        tmp.Order(y) = tmp.Order(x)
                        tmp.Order(x) = z
                    End Select
                    Continue Do
                End If
            End If
            score = tmpScore
            If tmpScore > maxScore Then
                maxScore = tmpScore
                tmp.CopyTo(tea)
            End If
        Loop
        tea.Export(poss)
        
		Dim ret(9) As String
        
        For i As Integer = 0 To UBound(ret)
            ret(i) = poss(i).ToString()
		Next i
        
		selectPositions = ret
		
	End Function
    
    Private Sub Shuffle(Of T)(vs() As T)
        For i As Integer = 0 To UBound(vs)
            Dim j As Integer = rand.Next(i, vs.Length)
            Dim v As T = vs(i)
            vs(i) = vs(j)
            vs(j) = v
        Next i
    End Sub
    
    Private Function MakeNew(N As Integer) As Team
        Dim tea As New Team(N)
        Shuffle(tea.Order)
        Shuffle(tea.Order)
        ' tea.FW = rand.Next(1 << 10)
        ' tea.DF = rand.Next(1 << 10) Xor tea.FW
        MakeNew = tea
    End Function
    
    Private Class Team
        Public Order() As Integer
        Public FW As Integer = 0
        Public DF As Integer = 0
        ' Public Score As Integer = 0
        ' Public Count As Integer = 0
        Sub New(N As Integer)
            ReDim Order(N - 1)
            For i As Integer = 0 To UBound(Order)
                Order(i) = i
            NExt i
        End Sub
        Private Sub New()
        End Sub
        Public Function GetCopy() As Team
            Dim t As New Team()
            ReDim t.Order(UBound(Order))
            CopyTo(t)
            GetCopy = t
        End Function
        Public Sub CopyTo(t As Team)
            Array.Copy(Order, t.Order, t.Order.Length)
            t.FW = FW
            t.DF = DF
        End Sub
        Public Function GetPosType(ByVal i As Integer) As Integer
            GetPosType = 1 - ((FW >> i) And 1) + ((DF >> i) And 1)
        End Function
        ' Public Function Compare(otr As Team) As Integer
            ' Compare = Score * otr.Count - otr.Score * Count
        ' End Function
        Public Sub Export(poss() As Position)
            For i As Integer = 0 To UBound(poss)
                poss(i).PType = GetPosType(i)
                poss(i).Players.Clear()
                For j As Integer = 0 To UBound(Order)
                    poss(i).Players.Add(Order((i * 3 + j) Mod Order.Length))
                Next j
            Next i
        End Sub
    End Class
        
    Private Function Simurate(tea As Team) As Integer
        Dim used As Integer = 0
        Dim flag As Integer = 0
        Dim score As Integer = 0
        Dim injure As Integer = rand.Next(5000)
        Dim rs(29) As Integer 
        Dim ms(9) As Integer
        Dim ps(9) As Integer
        Dim card(9) As Boolean
        For i As Integer = 0 To UBound(ps)
            ps(i) = i * 3
            ms(i) = tea.Order(ps(i))
            flag = flag Or (1 << ms(i))
        Next i
        used = flag
            
        For k As Integer = 1 To 5
            Dim atk As Integer = 0
            Dim def As Integer = 0
            Dim agg(9) As Integer
            For i As Integer = 0 To UBound(ms)
                If ms(i) < 0 Then Continue For
                Dim p As Player = players(ms(i))
                Select Case tea.GetPosType(i)
                Case 0
                    atk += p.ATK + p.ATK
                Case 1
                    atk += p.ATK
                    def += p.DEF
                Case 2
                    def += p.DEF + p.DEF
                End Select
                agg(i) = p.AGG
                rs(p.Index) = i
            Next i

            For Each grp As Group In groups
                If (flag And grp.Flag) <> grp.Flag Then Continue For
                For Each m As Integer In grp.Members
                    Dim p As Player = players(m)
                    Dim r As Integer = rs(m)
                    Select Case tea.GetPosType(r)
                    Case 0
                        atk += grp.ATK + grp.ATK
                    Case 1
                        atk += grp.ATK
                        def += grp.DEF
                    Case 2
                        def += grp.DEF + grp.DEF
                    End Select
                    agg(r) += grp.AGG
                Next m
            Next grp
            
            score += Math.Min(atk, def)
            
            For i As Integer = 0 To UBound(agg)
                Dim eject As Boolean = rand.Next(100000) < injure
                If rand.Next(100) < agg(i) Then
                    If card(i) Then
                        eject = True
                    Else
                        card(i) = True
                    End If
                End If
                If Not eject Then Continue For
                Dim j As Integer = ps(i)
                Do
                    j += 1
                    If j >= 30 Then j = 0
                    If j = ps(i) Then
                        j = -1
                        Exit Do
                    End If
                Loop While (used And (1 << tea.Order(j))) <> 0
                flag = flag Xor (1 << ms(i))
                card(i) = False
                If j < 0 Then
                    ms(i) = -1
                    ps(i) = -1
                Else
                    ps(i) = j
                    ms(i) = tea.Order(j)
                    flag = flag Or (1 << ms(i))
                    used = used Or flag
                End If
            Next i
        Next k
        
        ' tea.Score += score
        ' tea.Count += 1
        Simurate = score
        
    End Function
        
End Class