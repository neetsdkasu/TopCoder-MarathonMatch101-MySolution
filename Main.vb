Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim P As Integer = CInt(Console.ReadLine())
			Dim players(P - 1) As String
			For i As Integer = 0 To UBound(players)
				players(i) = Console.ReadLine()
			Next i
			
			Dim G As Integer = CInt(Console.ReadLine())
			Dim groups(G - 1) As String
			For i As Integer = 0 To UBound(groups)
				groups(i) = Console.ReadLine()
			Next i

			Dim wcl As New WorldCupLineup()
			Dim ret() As String = wcl.selectPositions(players, groups)

			Console.WriteLine(ret.Length)
			For Each r As String In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module